# Scala Days 2023 - Seattle: Teaching Domain Specific Languages in Scala

This repository contains the slides for [Andrea Mocci](https://andreamocci.gitlab.io)'s [talk at Scala Days 2023 - Seattle](https://scaladays.org/seattle-2023/teaching-domain-specific-languages-in-scala).

Most images are from [unsplash](https://unsplash.com) (see [license here](https://unsplash.com/license)) and icons are from [the noun project](https://thenounproject.com) (see license [here](https://thenounproject.com/legal/terms-of-use/#photo-licenses)). 